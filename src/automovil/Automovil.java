/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automovil;

import java.util.Scanner;

/**
 *
 * @author Itzanami Romero
 */
public class Automovil {
String color, marca, motor;
int asientos, ventanas, puertas;

    public Automovil(String color, String marca, String motor, int asientos, int ventanas, int puertas) {
        this.color = color;
        this.marca = marca;
        this.motor = motor;
        this.asientos = asientos;
        this.ventanas = ventanas;
        this.puertas = puertas;
    }
    public Automovil() {
        this.color = "-";
        this.marca = "-";
        this.motor = "-";
        this.asientos = 0;
        this.ventanas =0 ;
        this.puertas = 0;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public int getAsientos() {
        return asientos;
    }

    public void setAsientos(int asientos) {
        this.asientos = asientos;
    }

    public int getVentanas() {
        return ventanas;
    }

    public void setVentanas(int ventanas) {
        this.ventanas = ventanas;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }




float Acelerar(){
    float km=0, v=0, d=0, t=0;
    System.out.println("Dame la distancia y el tiempo");
    Scanner I = new Scanner(System.in);
    d= I.nextFloat();
    t= I.nextFloat();
    
    v= d/t;
    System.out.println("La velocidad es" + v);
    
    
    return v;
    
    
}



    /**
     * Método que regresa el valor del campo f
     */
    //si es void no hay return
    ;
    
    /**
     * @param args the command line arguments
     */

     void show (){
         System.out.printf("%s, \n %s, %d, %d,%d, %s", getColor(), getMarca(), getAsientos(), getPuertas(), getVentanas(), getMotor());
         
     }
    public static void main(String[] args) {

Automovil r1=new Automovil();
        r1.setAsientos(5);
        r1.setColor("azul");
        r1.setMarca("volkswagen");
        r1.setMotor("dos cilindros");
        r1.setPuertas(0);
        r1.setVentanas(0);
        r1.show();
        r1.Acelerar();

        // TODO code application logic here
    }
}
    




